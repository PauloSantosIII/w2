from datetime import *

def writer(filename, delimiter):
    def write_line(items):
        with open(filename, 'w') as f:
            for word in items:
                f.write(word, delimiter)
                f.close()
    
    return write_line

# ------------------------------------------

def logger(func):
    dt = datetime.strptime('dt', '%d-%m-%Y %H-%M').date()

    def wrapper(*args, **kwargs):
        if open('logger.log', 'r') is not None:
            with open('logger.log', 'a') as f_log:
                f_log.write(f'{dt} | {func} | {args} | {kwargs} | {result}')
                f_log.close()
                return func(*args, **kwargs)
        else:
            with open('logger.log', 'w') as f_log:
            f_log.write(f'{dt} | {func} | {args} | {kwargs} | {result}')
            f_log.close()
            return func(*args, **kwargs)

        return wrapper


@logger
def create_items(prefix, items=[]):
    result = {}
    count = 0
    for item in items:
        result['{}-{}'.format(prefix, count)] = item
        count += 1
    
    return result